#pragma once
#include "IComparable.h"

struct Cas : IComparable {
public:
	Cas(int _hodiny, int _minuty, int _sekundy);
	virtual int compareTo(IComparable* obj) const override;
	virtual std::string toString() const override;
private:
	int _hodiny;
	int _minuty;
	int _sekundy;
};
