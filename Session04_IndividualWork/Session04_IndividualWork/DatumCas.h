#ifndef DATUMCAS_H
#define DATUMCAS_H
#include "IComparable.h"
#include "Datum.h"
#include "Cas.h"	
class  DatumCas : public IComparable
{
public:
	DatumCas(const Datum& datum, const Cas& cas);
	std::string toString() const override;
	int compareTo(IComparable* obj) const override;
	~DatumCas() override;

private:
	Datum _date;
	Cas _time;
};

#endif
