#ifndef DATUM_H
#define DATUM_H

#include "IComparable.h"
#include "string"


class Datum : public IComparable
{
public:
	std::string toString() const override;
	int compareTo(IComparable* obj) const override;
	Datum(int aDay, int aMounth, int aYear);
	~Datum();
private:
	int _day;
	int _mounth;
	int _year;
};

#endif // !DATUM_H
