#include "DatumCas.h"
#include "IComparable.h"

#include <string.h>
#include <iostream>
using namespace std;
#define LENGTH 10


void seradPole(IComparable ** pole, int delkaPole) {
	bool prohod;
	for (int i = 0; i < delkaPole - 1; i++)
	{
		prohod = false;
		for (int j = 0; j < delkaPole - i - 1; j++)
		{
			if (pole[j]->compareTo(pole[j + 1]) == 1)
			{
				IComparable * temp = pole[j];
				pole[j] = pole[j + 1];
				pole[j + 1] = temp;
				prohod = true;
			}
		}

		if (prohod == false)
			break;
	}



}
int main(int argc, char** argv)
{
	IComparable** pole = new IComparable *[10];

	int randH, randM, randS;
	for (int i = 0; i < 10; i++)
	{
		randH = (std::rand() % (23 - 0 + 1)) + 0;
		randM = (std::rand() % (60 - 0 + 1)) + 0;
		randS = (std::rand() % (60 - 0 + 1)) + 0;
		pole[i] = new Cas{ randH, randM, randS };
	}

	for (int i = 0; i < 10; i++)
	{
		cout << pole[i]->toString() << endl;
	}

	seradPole(pole, 10);

	cout << "Setridene pole:" << endl;
	for (int i = 0; i < 10; i++)
	{
		cout << pole[i]->toString() << endl;
	}

	for (int i = 0; i < 10; i++)
	{
		delete pole[i];
	}
	delete[] pole;



	system("pause");

	return 0;

}