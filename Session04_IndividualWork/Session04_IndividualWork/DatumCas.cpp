#include "DatumCas.h"

DatumCas::DatumCas(const Datum & datum, const Cas & cas) : _date(datum), _time(cas)
{
}

std::string DatumCas::toString() const
{
	return _date.toString() + " " + _time.toString();
}

int DatumCas::compareTo(IComparable * obj) const
{
	DatumCas * cas2 = dynamic_cast<DatumCas*>(obj);
	if (cas2 == nullptr)
	{
		return -2;
	}



	IComparable* dateC = dynamic_cast<IComparable*>(obj);
	if (dateC == nullptr)
	{
		return -2;
	}


	IComparable* dateT = dynamic_cast<IComparable*>(obj);
	if (dateT == nullptr)
	{
		return -2;
	}


	int result1 = _date.compareTo(dateC);
	int result2 = _time.compareTo(dateT);

	if (result1 != 0)
	{

		return result1;
	}


	if (result2 != 0)
	{

		return result2;
	}

	return 0;

}

DatumCas::~DatumCas()
{
}
