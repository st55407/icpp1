#include "Cas.h"
#include <string>
using namespace std;

Cas::Cas(int _hodiny, int _minuty, int _sekundy)
{
	if (_hodiny >= 0 && _hodiny <= 23)
	{
		this->_hodiny = _hodiny;
	}  
	else
	{
		this->_hodiny = 0;
	}
	if (_minuty >= 0 && _minuty <= 59)
	{
		this->_minuty = _minuty;
	}
	else
	{
		this->_minuty = 0;
	}
	if (_sekundy >= 0 && _sekundy <= 59)
	{
		this->_sekundy = _sekundy;
	}
	else
	{
		this->_sekundy = 0;
	}
}

int Cas::compareTo(IComparable* obj) const
{
	Cas* cas = (Cas*)obj;
	if (this->_hodiny > cas->_hodiny)
	{
		return 1;
	}
	else if (this->_hodiny < cas->_hodiny)
	{
		return -1;
	}
	else
	{
		if (this->_minuty > cas->_minuty)
		{
			return 1;
		}
		else if (this->_minuty < cas->_minuty)
		{
			return -1;
		}
		else
		{
			if (this->_sekundy > cas->_sekundy)
			{
				return 1;
			}
			else if (this->_sekundy < cas->_sekundy)
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}
	}
}

std::string Cas::toString() const
{
	std::string str = "";
	str += to_string(this->_hodiny);
	str += ":";
	str += to_string(this->_minuty);
	str += ":";
	str += to_string(this->_sekundy);
	return str;
}
