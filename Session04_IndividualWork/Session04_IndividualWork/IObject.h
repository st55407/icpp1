#ifndef IOBJECT_H 



#include <iostream>
#include <string>

struct IObject
{
	virtual ~IObject();
	IObject() {}
	virtual std::string toString() const = 0;



};

#endif // !
