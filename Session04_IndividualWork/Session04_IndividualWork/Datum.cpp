#include "Datum.h"

std::string Datum::toString() const
{
	return std::to_string(_year) + " " + std::to_string(_mounth) + "." + std::to_string(_day) + ".";
}

int Datum::compareTo(IComparable * obj) const
{

	Datum* cas2 = dynamic_cast<Datum*>(obj);
	if (cas2 == nullptr)
		return -2; //Chybovy stav

	if (_year > cas2->_year)
		return 1;

	if (_year < cas2->_year)
		return -1;

	if (_mounth > cas2->_mounth)
		return 1;

	if (_mounth < cas2->_mounth)
		return -1;

	if (_day > cas2->_day)
		return 1;

	if (_day < cas2->_day)
		return -1;

	return 0;

}

Datum::Datum(int aDay, int aMounth, int aYear) : _day(aDay), _mounth(aMounth), _year(aYear)
{
}

Datum::~Datum()
{
}
