#ifndef ICOMPARABLE_H



#include <iostream>
#include "IObject.h"


struct IComparable :public IObject
{
	virtual ~IComparable() {};
	IComparable() {}
	virtual int compareTo(IComparable* obj) const = 0;

};


#endif // !1
