#ifndef HRA_H
#include "Object.h"
#include "PohyblivyObject.h"
#include "StatickyObject.h"
#define ARRAY_SIZE 200
class Hra	
{
public:
	Hra();
	~Hra();
	void vlozObject(Object* obj);
	int* najdiStatickychObject(double xmin, double xmax, double ymin, double ymax);
	PohyblivyObject** najdiPohyblivyObjectyVoblasti(double x, double y, double r);
	PohyblivyObject** najdiPohyblivyObjectyVoblasti(double x, double y, double r, double umin, double umax);

private:
	Object** objekty;
	int pocet = 0;
};


#endif 

 