#ifndef POHYBLIVY_OBJEKT_H
#define  POHYBLIVY_OBJEKT_H
#include "Object.h"
#define _USE_MATH_DEFINES
#include<math.h>
#include <stdexcept> 

class PohyblivyObject : public Object
{
public:
	PohyblivyObject(int aId, double aUhel);
	double getUhelNatoceni() const;
	void setUhelNatoceni(double aUhel);
private:
	double uhelNatoceni;
};

#endif // OBJECT_H
