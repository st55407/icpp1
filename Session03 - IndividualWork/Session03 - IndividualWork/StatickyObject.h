#ifndef STATICKY_OBJECT_H
#define STATICKY_OBJECT_H
#include "Object.h"
enum  TypPrekazky
{
	Skala,
VelkaRostlina,
MalaRostlina};


class StatickyObject : public Object
{
public:
	StatickyObject(int aId, TypPrekazky prekazka);
		TypPrekazky getPrekazka();

private:
	TypPrekazky prekazka;
};


#endif 

