#include "StatickyObject.h"

StatickyObject::StatickyObject(int Id, TypPrekazky prekazka) : Object(Id) {

	this->prekazka = prekazka;
};

TypPrekazky StatickyObject::getPrekazka()
{
	return this->prekazka;
}
