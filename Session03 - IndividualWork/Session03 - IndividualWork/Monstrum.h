#ifndef MONSTRUM_H
#define MONSTRUM_H
#include "PohyblivyObject.h"
class Monstrum : public PohyblivyObject
{
public:
	Monstrum(int aId, double aUhel, int aHp, int aMaxHp);
	
	void setHp(int aHp);
	void setMaxHp(int aMaxHp);
	int getMaxHp();
	int getHp();
private:
	int hp;
	int maxHp;

};

#endif 
