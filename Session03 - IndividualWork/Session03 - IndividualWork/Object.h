#ifndef OBJECT_H
#define OBJECT_H
class Object
{
public:
	Object(int aId);
	virtual ~Object();
	int  getId();
	double getX() const;
	double getY() const;
	void setX( double aX);
	void setY( double aY);
private:
	int id;
	double x;
	double y;
};

#endif // OBJECT_H
