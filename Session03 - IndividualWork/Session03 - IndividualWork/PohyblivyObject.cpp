#include "PohyblivyObject.h"
#include<cmath>
#include<stdexcept>
#define MINIMUM_ANGLE 0
#define MAXIMUM_ANGLE (2*(std::atan(1)*4))


PohyblivyObject::PohyblivyObject(int aId, double aUhel) : Object(aId)
{
	setUhelNatoceni(aUhel);
}

double PohyblivyObject::getUhelNatoceni() const
{
	return this->uhelNatoceni;
}



void PohyblivyObject::setUhelNatoceni(double aUhel)
{
	if (aUhel >= MINIMUM_ANGLE && aUhel <= MAXIMUM_ANGLE)
	{
		this->uhelNatoceni = aUhel;
	}
	else
	{
		throw std::invalid_argument("The rotation angle value you've entered is invalid.");
	}
}
