#include "Hra.h"
#include <math.h>
#include <string.h>
#include <iostream>
using namespace std;

Hra::Hra()
{
	objekty = new Object *[ARRAY_SIZE];

}

Hra::~Hra()
{
	delete[]objekty;
}

void Hra::vlozObject(Object * obj)
{
	objekty[pocet] = obj;
	pocet++;
}

int * Hra::najdiStatickychObject(double xmin, double xmax, double ymin, double ymax)
{
	int poleVel = 0;

	for (int i = 0; i < pocet; i++)
	{

		Object * obj = objekty[i];

		StatickyObject* staticObj = dynamic_cast<StatickyObject*>(obj);
		if (staticObj != nullptr)
		{

			if (staticObj->getX() > xmin && staticObj->getX() < xmax && staticObj->getY() > ymin && staticObj->getY() < ymax)
			{

				poleVel++;
			}
		}
	}

	int *poleInt = new int[poleVel];
	int index = 0;
	for (int i = 0; i < pocet; i++)
	{
		Object * obj = objekty[i];
		StatickyObject * statObj = dynamic_cast<StatickyObject*>(obj);

		if (statObj != nullptr)
		{

			if (statObj->getX() > xmin && statObj->getX() < xmax && statObj->getY() > ymin && statObj->getY() < ymax)
			{
				poleInt[index] = obj->getId();
				index++;
			}
		}
	}
	return poleInt;
}

PohyblivyObject ** Hra::najdiPohyblivyObjectyVoblasti(double x, double y, double r)
{
	int poleVel = 0;

	for (int i = 0; i < pocet; i++)
	{
		Object * obj = objekty[i];
		PohyblivyObject * pohybObj = dynamic_cast<PohyblivyObject*>(obj);
		if (pohybObj == nullptr)
		{
			continue;
		}
		if ((pow(pohybObj->getX() - x, 2) + pow(pohybObj->getY() - y, 2) < pow(r, 2)))
		{
			poleVel++;

		}
	}

	PohyblivyObject ** polePohybObj = new PohyblivyObject *[poleVel];
	int index = 0;

	for (int i = 0; i < pocet; i++)
	{
		Object * obj = objekty[i];
		PohyblivyObject * pohybObj = dynamic_cast<PohyblivyObject*>(obj);

		if (pohybObj == nullptr)
		{
			continue;
		}
		if ((pow(pohybObj->getX() - x, 2) + pow(pohybObj->getY() - y, 2) < pow(r, 2)))
		{
			polePohybObj[index] = pohybObj;
			index++;
		}
	}
	return polePohybObj;
}

PohyblivyObject ** Hra::najdiPohyblivyObjectyVoblasti(double x, double y, double r, double umin, double umax)
{
	int poleVel = 0;

	for (int i = 0; i < pocet; i++)
	{
		Object * obj = objekty[i];
		PohyblivyObject * pohybObj = dynamic_cast<PohyblivyObject*>(obj);
		if (pohybObj == nullptr)
		{
			continue;
		}
		if ((pow(pohybObj->getX() - x, 2) + pow(pohybObj->getY() - y, 2) < pow(r, 2)))
		{
			poleVel++;
		}
	}

	PohyblivyObject ** polePohybObj = new PohyblivyObject *[poleVel];
	int index = 0;

	for (int i = 0; i < pocet; i++)
	{
		Object * obj = objekty[i];
		PohyblivyObject * pohybObj = dynamic_cast<PohyblivyObject*>(obj);

		if (pohybObj == nullptr)
		{
			continue;
		}
		if ((pow(pohybObj->getX() - x, 2) + pow(pohybObj->getY() - y, 2) < pow(r, 2) && pohybObj->getUhelNatoceni() > umin && pohybObj->getUhelNatoceni() < umax))
		{
			polePohybObj[index] = pohybObj;
			index++;
		}
	}
	return polePohybObj;
}
