#include "Monstrum.h"

Monstrum::Monstrum(int aId, double aUhel, int aHp, int aMaxHp) : hp(aHp), maxHp(aMaxHp),PohyblivyObject(aId, aUhel)
{
	
}

void Monstrum::setHp(int aHp)
{
	this->hp = aHp;
}

void Monstrum::setMaxHp(int aMaxHp)
{

	this->maxHp = aMaxHp;
}

int Monstrum::getMaxHp()
{
	return this->maxHp;
}

int Monstrum::getHp()
{
	return this->hp;
}
