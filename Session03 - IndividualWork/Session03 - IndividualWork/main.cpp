

#include <string.h>
#include <time.h>
#include <iostream>
#include "Hra.h"
#include  "Monstrum.h"
#include  "Object.h"
#include "PohyblivyObject.h"
#include "StatickyObject.h"
using namespace std;
int main(int argc, char** argv)
{

	Hra hra =  Hra();
	Object * obj = new Object(1);
	obj->setX(5);
	obj->setY(10);
	StatickyObject * statObj = new StatickyObject(5,Skala);
	statObj->setX(20);
	statObj->setY(68);
	PohyblivyObject * pohybObj = new PohyblivyObject(7, 1);
	pohybObj->setX(25);
	pohybObj->setY(68);
	Monstrum * monstr = new Monstrum(10, 2, 2, 100);
	monstr->setX(25);
	monstr->setY(68);

	hra.vlozObject(obj);
	hra.vlozObject(statObj);
	hra.vlozObject(pohybObj);
	hra.vlozObject(monstr);

	std::cout << hra.najdiStatickychObject(1, 50, 10, 90)[0] << std::endl;
	std::cout << hra.najdiPohyblivyObjectyVoblasti(1, 1, 1000)[0]->getId() << std::endl;
	std::cout << hra.najdiPohyblivyObjectyVoblasti(1, 1, 1000, 0, 2)[0]->getId() << std::endl;

	time_t startTime = time(nullptr);

	for (size_t i = 0; i < 100000; i++)
	{
		Object * obj;
		if (rand() % 2 == 0)
		{
			PohyblivyObject * pohObj = new PohyblivyObject(i,rand() % 100 + 100);
			pohObj->setX(rand() % 100 + 100);
			pohObj->setX(rand() % 100 + 100);
		}

	}
	time_t endTime = time(nullptr);
	char * startBuffer = new char[20];
	char * endBuffer = new char[20];
	delete obj;
	delete statObj;
	delete pohybObj;
	delete monstr;

	system("pause");

	return 0;

}
