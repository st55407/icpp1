#ifndef HUB_H
#define HUB_H

#include "ASitovyPrvek.h"

struct Hub : ASitovyPrvek {
public:
	Hub(int maximumPripojenychPrvku);
	~Hub();
	virtual void Provadej() override;
	virtual void Pripoj(ASitovyPrvek* aSitovyPrvek) override;

private:
	virtual void ZpracujPrichoziZpravu(ZpravaPort zp) override;

	ASitovyPrvek** pripojennePrvky;
	int maximumPripojenychPrvku;
	Fronta<Zprava*> zpracovaneZpravy;
};
#endif // !HUB_H
