#ifndef UZEL_H
#define UZEL_H

#include "ASitovyPrvek.h"

struct Uzel : ASitovyPrvek
{
public:
	Uzel(std::string adresa);
	~Uzel();
	void PripravZpravuKOdeslani(std::string cil, std::string obsah);
	virtual void Provadej() override;
	virtual void Pripoj(ASitovyPrvek* aSitovyPrvek) override;

private:
	virtual void ZpracujPrichoziZpravu(ZpravaPort zp) override;
	std::string adresa;
	ASitovyPrvek* pripojenyPrvek;
	Fronta<Zprava*> odchoziZpravy;
};
#endif // !UZEL_H
