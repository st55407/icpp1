#include "Uzel.h"
#include "Sit.h"
#include <iostream>
using namespace std;

int Sit::idZpravy = 0;

Uzel::Uzel(std::string adresa)
{
	this->adresa = adresa;
}

Uzel::~Uzel()
{

}


void Uzel::PripravZpravuKOdeslani(std::string cil, std::string obsah)
{
	Zprava* novaZprava = new Zprava(Sit::DejNoveId(), adresa, cil, obsah);
	odchoziZpravy.Vloz(novaZprava);
}

void Uzel::Provadej()
{
	while (!odchoziZpravy.JePrazdna())
	{
		Zprava* zpravaKOdeslani = odchoziZpravy.Odeber();
		pripojenyPrvek->VlozPrichoziZpravu(zpravaKOdeslani, this);
	}
	while (!prichoziZpravy.JePrazdna())
	{
		ZpravaPort prichoziZprava = prichoziZpravy.Odeber();
		ZpracujPrichoziZpravu(prichoziZprava);
	}
}

void Uzel::Pripoj(ASitovyPrvek* aSitovyPrvek)
{
	pripojenyPrvek = aSitovyPrvek;
}

void Uzel::ZpracujPrichoziZpravu(ZpravaPort zp)
{
	if (zp.zprava->adrCil != adresa)
	{
		return;
	}

	cout << adresa << " RECV id: " << zp.zprava->id << " src: " << zp.zprava->adrZdroj << " msg:" << zp.zprava->obsah << endl;

	if (zp.zprava->obsah == "ping")
	{
		PripravZpravuKOdeslani(zp.zprava->adrZdroj, "pong");
	}
}
