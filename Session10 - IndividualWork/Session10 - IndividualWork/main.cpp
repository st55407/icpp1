
#define _CRTDBG_MAP_ALLOC
#include "Hub.h"
#include "Sit.h"
#include "Uzel.h"

#define propoj(a,b) a->Pripoj(b); b->Pripoj(a);
#define propojs(a,b,c) propoj(a,b); c->Pripoj(a); c->Pripoj(b);
using namespace std;

int main()
{
	
	Sit* virtualniSit = new Sit{ };

	Hub* hub1 = new Hub{ 8 };
	Hub* hub2 = new Hub{ 8 };
	Hub* hub3 = new Hub{ 8 };
	Hub* hub4 = new Hub{ 16 };
	Hub* hub5 = new Hub{ 24 };

	Uzel* uzel1 = new Uzel{ "a1" };
	Uzel* uzel2 = new Uzel{ "a2" };
	Uzel* uzel3 = new Uzel{ "a3" };


	propojs(uzel1, hub1, virtualniSit);
	propojs(uzel2, hub3, virtualniSit);
	propojs(uzel3, hub5, virtualniSit);

	propojs(hub1, hub2, virtualniSit);
	propojs(hub2, hub3, virtualniSit);
	propojs(hub1, hub4, virtualniSit);
	propojs(hub4, hub5, virtualniSit);
	propojs(hub5, hub2, virtualniSit);

	uzel1->PripravZpravuKOdeslani("a2", "ping");
	uzel1->PripravZpravuKOdeslani("a3", "ping");

	for (int i = 0; i < 90; i++)
		virtualniSit->ProvadejVse();
	return 0;

}
