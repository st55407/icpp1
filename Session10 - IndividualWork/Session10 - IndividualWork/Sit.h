#pragma once

#include "ASitovyPrvek.h"
struct Sit
{

public:
	static int DejNoveId();
	void Pripoj(ASitovyPrvek* sitovyPrvek);
	void ProvadejVse();

private:
	static int idZpravy;
	Fronta<ASitovyPrvek*> sitovePrvky;
};

