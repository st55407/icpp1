#include "Hub.h"

Hub::Hub(int max)
{

}

Hub::~Hub()
{
	delete[] pripojennePrvky;
}

void Hub::Provadej()
{
	while (!prichoziZpravy.JePrazdna())
	{
		ZpravaPort zp = prichoziZpravy.Odeber();
		ZpracujPrichoziZpravu(zp);
	}
}

void Hub::Pripoj(ASitovyPrvek* aSitovyPrvek)
{
	for (int i = 0; i < maximumPripojenychPrvku; i++)
	{
		if (pripojennePrvky[i] == nullptr)
		{
			pripojennePrvky[i] = aSitovyPrvek;
			break;
		}
	}
}

void Hub::ZpracujPrichoziZpravu(ZpravaPort zp)
{
	if (zpracovaneZpravy.Obsahuje(zp.zprava)) {
		return;
	}

	for (int i = 0; i < maximumPripojenychPrvku; i++)
	{
		if (pripojennePrvky[i] != nullptr)
		{
			if (pripojennePrvky[i] != zp.port)
			{
				pripojennePrvky[i]->VlozPrichoziZpravu(zp.zprava, this);
			}
		}
	}
	zpracovaneZpravy.Vloz(zp.zprava);
}
