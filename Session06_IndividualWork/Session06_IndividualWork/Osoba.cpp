#include "Osoba.h"
#include "Osoba.h"
std::ostream& operator<<(std::ostream & output, const Osoba & os)
{
	output << os.jmeno << "\n" << os.prijmeni << "\n" << os.datumNarozeni << "\n" << os.eAdresa << std::endl;
	return output;
}

std::istream & operator>>(std::istream & input, Osoba & os)
{
	input >> os.jmeno;
	input >> os.prijmeni;
	input >> os.eAdresa;
	input >> os.datumNarozeni;

	return input;
}

std::ostream & operator<<(std::ostream & output, const Datum& dat)
{
	output << dat.den << "." << dat.mesic << "." << dat.rok << std::endl;
	return output;
}

std::istream & operator>>(std::istream & input, Datum& dat)
{
	input >> dat.den;
	input >> dat.mesic;
	input >> dat.rok;
	return input;
}


std::ostream & operator<<(std::ostream & output, const Adresa & adr)
{
	output << adr.mesto << "\n" << adr.ulice << "\n" << adr.psc << std::endl;
	return output;
}

std::istream & operator>>(std::istream & input, Adresa & adr)
{
	input >> adr.ulice;
	input >> adr.mesto;
	input >> adr.psc;
	return input;
}

