#ifndef OSOBA_H
#define OSOBA_H

#include <string>
#include <iostream>

struct Datum {
	int den;
	int mesic;
	int rok;
};

struct Adresa {
	std::string ulice;
	std::string mesto;
	int psc;
};

struct Osoba {
	std::string jmeno;
	std::string prijmeni;
	Adresa eAdresa;
	Datum datumNarozeni;
};

std::ostream& operator<<(std::ostream& output, const Osoba& p);
std::istream& operator>>(std::istream& input, Osoba& p);

#endif 
