#include "Adresa.h"


std::ostream & operator<<(std::ostream & output, const Adresa & a)
{
	output << a.mesto << " " << a.ulice << " " << a.psc << std::endl;
	return output;
}

std::istream & operator>>(std::istream & input, Adresa & a)
{
	input >> a.mesto;
	input >> a.ulice;
	input >> a.psc;
	return input;
}

void ulozBin(std::ofstream & output, const Adresa & a)
{
	int delka = a.mesto.size() + 1;
	output.write((const char*)&delka, sizeof(int));
	output.write(reinterpret_cast<char*>(&delka), sizeof(int));
	output.write(a.mesto.c_str(),delka);

	delka = a.ulice.size() + 1;
	output.write((const char*)&delka, sizeof(int));
	output.write(reinterpret_cast<char*>(&delka), sizeof(int));
	output.write(a.ulice.c_str(), delka);

	output.write((const char*)&a.psc, sizeof(int));

}

void nactiBin(std::ifstream & input, Adresa & a)
{


	char* pom;
	int delka = 0;

	input.read((char*) & (delka), sizeof(int));
	input.read(reinterpret_cast<char*>(&delka), sizeof(int));
	pom = new char[delka];
	input.read(pom, delka);
	a.mesto.append(pom, delka);

	input.read((char*)&(delka), sizeof(int));
	input.read(reinterpret_cast<char*>(&delka), sizeof(int));
	pom = new char[delka];
	input.read(pom, delka);
	a.ulice.append(pom, delka);

	input.read((char*) & (a.psc), sizeof(int));

}
