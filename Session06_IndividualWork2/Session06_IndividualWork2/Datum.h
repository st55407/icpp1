#ifndef DATUM_H
#define  DATUM_H

#include <string>
#include <iostream>
#include <fstream>


struct Datum {
	int den;
	int mesic;
	int rok;
};


std::ostream& operator<<(std::ostream& output, const Datum& d);
std::istream& operator>>(std::istream& input, Datum& d);



void ulozBin(std::ofstream& output, const Datum& d);
void nactiBin(std::ifstream& input, Datum& d);
#endif 