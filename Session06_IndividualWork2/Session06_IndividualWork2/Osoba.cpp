#include "Osoba.h"
#include <iostream>
#include <ostream>
#include <fstream>


std::ostream& operator<<(std::ostream & output, const Osoba& os)
{
	output << os.jmeno << "\n" << os.prijmeni << "\n" << os.datumNarozeni << "\n" << os.eAdresa << std::endl;
	return output;
}

std::istream & operator>>(std::istream & input, Osoba & os)
{
	input >> os.jmeno;
	input >> os.prijmeni;
	input >> os.datumNarozeni;
	input >> os.eAdresa;

	return input;
}

void ulozBin(std::ofstream & output, const Osoba & os)
{

	int delka = os.jmeno.size() + 1;
	output.write((const char*)&delka, sizeof(int));
	output.write(reinterpret_cast<char*>(&delka), sizeof(int));
	output.write(os.jmeno.c_str(), delka);

	delka = os.prijmeni.size() + 1;
	output.write((const char*)&delka, sizeof(int));
	output.write(reinterpret_cast<char*>(&delka), sizeof(int));
	output.write(os.prijmeni.c_str(), delka);

	output << os.eAdresa;
	output << os.datumNarozeni;

}

void nactiBin(std::ifstream & input, Osoba & os)

{
	int delka = 0;
	char* pom;
	input.read((char*) & (delka), sizeof(int));
	input.read(reinterpret_cast<char*>(&delka), sizeof(int));
	pom = new char[delka];
	input.read(pom, delka);
	os.jmeno.append(pom, delka);

	input.read((char*) & (delka), sizeof(int));
	input.read(reinterpret_cast<char*>(&delka), sizeof(int));
	pom = new char[delka];
	input.read(pom, delka);
	os.prijmeni.append(pom, delka);

	input >> os.eAdresa;
	input >> os.datumNarozeni;


}



