#include "Datum.h"
std::ostream & operator<<(std::ostream & output, const Datum & d)
{
	output << d.den << " " << d.mesic << " " << d.rok << std::endl;
	return output;
}

std::istream & operator>>(std::istream & input, Datum & d)
{
	input >> d.den;
	input >> d.mesic;
	input >> d.rok;
	return input;
}

void ulozBin(std::ofstream& output, const Datum & d)
{
	

	output.write((const char*)&d.den, sizeof(int));
	output.write((const char*)&d.mesic, sizeof(int));
	output.write((const char*)&d.rok, sizeof(int));

}

void nactiBin(std::ifstream & input, Datum & d)
{
	input.read((char*)&d.den, sizeof(int));
	input.read((char*)&d.mesic, sizeof(int));
	input.read((char*)&d.rok, sizeof(int));

}
