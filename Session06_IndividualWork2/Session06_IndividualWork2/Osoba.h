#ifndef OSOBA_H
#define OSOBA_H
#include "Adresa.h"
#include "Datum.h"
#include <iostream>
#include <ostream>
#include <fstream>

struct Osoba {
	std::string jmeno;
	std::string prijmeni;
	Adresa eAdresa;
	Datum datumNarozeni;
};

std::ostream& operator<<(std::ostream& output, const Osoba& os);
std::istream& operator>>(std::istream& input, Osoba& os);

void ulozBin(std::ofstream& output, const Osoba& os);
void nactiBin(std::ifstream& input, Osoba& os);



#endif  OSOBA_H
