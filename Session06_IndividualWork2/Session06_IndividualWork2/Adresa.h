#ifndef ADRESA_H
#define ADRESA_H

#include <string>
#include <iostream>
#include <fstream>



struct Adresa {
	std::string ulice;
	std::string mesto;
	int psc;
};



std::ostream& operator<<(std::ostream& output, const Adresa& a);
std::istream& operator>>(std::istream& input, Adresa& a);

void ulozBin(std::ofstream& output, const Adresa& a);
void nactiBin(std::ifstream& input, Adresa& a);

#endif 