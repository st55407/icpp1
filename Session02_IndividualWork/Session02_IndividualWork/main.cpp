#include "Pokladna.h"
#include "Uctenka.h"

#include <string.h>
#include <iostream>
using namespace std;
int main(int argc, char** argv)
{

	Pokladna* pokl = new Pokladna();
	Uctenka uctenka1 = pokl->vystavUctenku(111.4, 29);
	Uctenka uctenka2 = pokl->vystavUctenku(22.2, 25);
	Uctenka uctenka3 = pokl->vystavUctenku(123.9, 15);

	cout << "Uctenka c.: " << uctenka1.getCisloUctenky() << ", castka: " << uctenka1.getCastka() << ", sazba DPH: " << uctenka1.getDph() << " %." << endl;
	cout << "Uctenka c.: " << uctenka2.getCisloUctenky() << ", castka: " << uctenka2.getCastka() << ", sazba DPH: " << uctenka2.getDph() << " %." << endl;
	cout << "Uctenka c.: " << uctenka3.getCisloUctenky() << ", castka: " << uctenka3.getCastka() << ", sazba DPH: " << uctenka3.getDph() << " %." << endl;





	Uctenka uctenka4 = pokl->dejUctenku(1001);
	Uctenka uctenka5 = pokl->dejUctenku(1002);
	Uctenka uctenka6 = pokl->dejUctenku(1003);

	cout << endl;
	cout << "Uctenka c.: " << uctenka4.getCisloUctenky() << ", castka: " << uctenka4.getCastka() << ", sazba DPH: " << uctenka4.getDph() << " %." << endl;
	cout << "Uctenka c.: " << uctenka5.getCisloUctenky() << ", castka: " << uctenka5.getCastka() << ", sazba DPH: " << uctenka5.getDph() << " %." << endl;
	cout << "Uctenka c.: " << uctenka6.getCisloUctenky() << ", castka: " << uctenka6.getCastka() << ", sazba DPH: " << uctenka6.getDph() << " %." << endl;

	cout << "Celkova castka (bez DPH): " << pokl->dejCastku() << endl;
	cout << "Celkova castka (s DPH): " << pokl->dejCastkuDph() << endl;


	system("pause");

	return 0;

}
