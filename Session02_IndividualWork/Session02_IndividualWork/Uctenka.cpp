#include "Uctenka.h"

int Uctenka::getCisloUctenky()
{
	return this->cisloUctenky;
}

double Uctenka::getCastka()
{
	return this->castka;
}

double Uctenka::getDph()
{
	return this->dph;
}

void Uctenka::setCisloUctenky(int a)
{
	this->cisloUctenky = a;
}

void Uctenka::setCastka(double x)
{
	this->castka = x;
}

void Uctenka::setDph(double y)
{
	this->dph = y;
}