#ifndef POKLADNA_H
#define POKLADNA_H
#include "Uctenka.h"
#include <string>

class Pokladna
{
public:
	Pokladna();
	~Pokladna();
	Uctenka& vystavUctenku(double castka, double dph);
	Uctenka& dejUctenku(int cislo);
	double dejCastku();
	double dejCastkuDph();

private:
	Uctenka* uctenky;
	static int pocetUctenek;
	 static int citacId;
};



#endif