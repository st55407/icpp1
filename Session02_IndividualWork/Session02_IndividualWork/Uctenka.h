#ifndef UCTENKA_H
#define UCTENKA_H

#include <string>


struct Uctenka
{
public:

	int getCisloUctenky();
	double getCastka();
	double getDph();
	void setCisloUctenky(int a);
	void setCastka(double x);
	void setDph(double y);
private:
	int cisloUctenky;
	double castka;
	double dph;
};



#endif