#include "Pokladna.h"

int Pokladna::pocetUctenek = 0;
#define ID_COUNTER 1000
int Pokladna::citacId = ID_COUNTER;
Pokladna::Pokladna() {
	this->uctenky = new Uctenka[10];

}

Pokladna::~Pokladna() {

	delete[] uctenky;


}

Uctenka& Pokladna::vystavUctenku(double castka, double dph) {

	this->uctenky[pocetUctenek].setCastka(castka);
	this->uctenky[pocetUctenek].setDph(dph);
	this->uctenky[pocetUctenek].setCisloUctenky(citacId + pocetUctenek + 1);
	pocetUctenek++;

	return  uctenky[pocetUctenek - 1];
}

Uctenka& Pokladna::dejUctenku(int cisloU) {
	for (size_t i = 0; i < pocetUctenek; i++)
	{
		if (uctenky[i].getCisloUctenky() == cisloU)
		{
			return uctenky[i];
		}

	}
	return uctenky[0];

}

double Pokladna::dejCastku() {
	double celkova = 0;
	for (size_t i = 0; i < pocetUctenek; i++)
	{
		celkova += uctenky[i].getCastka();
	}
	return celkova;
}

double Pokladna::dejCastkuDph() {
	double celkova = 0;
	for (size_t i = 0; i < pocetUctenek; i++)
	{
		celkova += uctenky[i].getCastka() + (1 + uctenky[i].getDph() / 100);
	}
	return celkova;
}

