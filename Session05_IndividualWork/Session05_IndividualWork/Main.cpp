#include <time.h>
#include <iostream>
#include "Osoba.h"
#include "telefoniSeznam.h"

int main(int argc, char* argv) {

	Model::TelefonniSeznam* ts = new Model::TelefonniSeznam();
	Entity::Osoba* osoba = new Entity::Osoba("Pepa","125478", 1);
	Entity::Osoba* osoba2 = new Entity::Osoba("Adam", "874561", 10);
	Entity::Osoba* osoba3 = new Entity::Osoba("Michal", "541233", 150);

	ts->pridejOsobu(*osoba);
	ts->pridejOsobu(*osoba2);
	ts->pridejOsobu(*osoba3);
	try
	{
		ts->najdiTelefon("Pepa");
	}
	catch (const std::exception e)
	{
		std::cout << e.what() << std::endl;
	}

	try {
		ts->najdiTelefon(150);
	}
	catch (std::exception e) {
		std::cout << e.what() << std::endl;
	}

	system("pause");
	return 0;

}
