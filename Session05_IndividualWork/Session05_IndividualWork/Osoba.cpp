#include "Osoba.h"

Entity::Osoba::Osoba()
{
}

Entity::Osoba::Osoba(std::string aJmeno, std::string aTelefon, int aId) : jmeno(aJmeno), telefon(aTelefon), id(aId)
{
}

Entity::Osoba::~Osoba()
{
}

void Entity::Osoba::setJmeno(std::string aJmeno)
{
	this->jmeno = aJmeno;
}

void Entity::Osoba::setTel(std::string aTelefon)
{
	this->telefon = aTelefon;
}

void Entity::Osoba::setId(int aId)

{
	this->id = aId;
}

std::string Entity::Osoba::getJmeno() const
{
	return jmeno;
}

std::string Entity::Osoba::getTel() const
{
	return telefon;
}

int Entity::Osoba::getId() const
{
	return this->id;
}
