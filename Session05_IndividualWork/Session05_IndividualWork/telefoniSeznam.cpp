#include "telefoniSeznam.h"
#include "Osoba.h"

Model::PrvekSeznamu::PrvekSeznamu()
{
}

Model::PrvekSeznamu::PrvekSeznamu(PrvekSeznamu * aDalsi, Entity::Osoba aData) : dalsi(aDalsi), data(aData)
{
	
}

Model::PrvekSeznamu::~PrvekSeznamu()
{
}

Model::TelefonniSeznam::TelefonniSeznam()
{
	zacatek = nullptr;
	aktualni = nullptr;
	pocet = 0;
}

Model::TelefonniSeznam::~TelefonniSeznam()
{
	while (zacatek != nullptr)
	{
		PrvekSeznamu* pom = zacatek->dalsi;
		delete zacatek;
		zacatek = pom;

	}

}

void Model::TelefonniSeznam::pridejOsobu(Entity::Osoba o)
{
	if (pocet == 0)
	{
		PrvekSeznamu*prvek = new PrvekSeznamu();
		prvek->data.setId(o.getId());
		prvek->data.setJmeno(o.getJmeno());
		prvek->data.setTel(o.getTel());
		zacatek = prvek;
		zacatek->dalsi = nullptr;
		aktualni = zacatek;
		pocet++;
	}
	else {
		PrvekSeznamu*prvek = new PrvekSeznamu();
		prvek->data.setId(o.getId());
		prvek->data.setJmeno(o.getJmeno());
		prvek->data.setTel(o.getTel());
		aktualni->dalsi = prvek;
		aktualni = prvek;
		pocet++;
	}


}

std::string Model::TelefonniSeznam::najdiTelefon(std::string aJmeno) const
{
	if (aJmeno.size() == 0) {
		throw std::invalid_argument("Nenalezen");
	}
	PrvekSeznamu* prvek = zacatek;
	while (prvek != nullptr) {
		if (prvek->data.getJmeno() == aJmeno) {
			return prvek->data.getTel();
		}
		prvek = prvek->dalsi;
	}
	throw std::invalid_argument("Prvek neexistuje");


}

std::string Model::TelefonniSeznam::najdiTelefon(int aId) const
{

	if (aId < 0) {
		throw std::invalid_argument("Zadny parametr");
	}
	PrvekSeznamu* prvek = zacatek;
	while (prvek != nullptr) {
		if (prvek->data.getId() == aId) {
			return prvek->data.getTel();
		}
		prvek = prvek->dalsi;
	}
	throw std::invalid_argument("Neexistuje");


}
