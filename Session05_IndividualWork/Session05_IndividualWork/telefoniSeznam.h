#ifndef telefoniSeznam_H
#define telefoniSeznam_H
#include "Osoba.h"
#include <string>


namespace Model {
	struct PrvekSeznamu {
	public:
		PrvekSeznamu* dalsi;
		Entity::Osoba data;
		PrvekSeznamu();
		PrvekSeznamu(PrvekSeznamu* aDalsi, Entity::Osoba aData);
		~PrvekSeznamu();
	};

	struct TelefonniSeznam {
	private:
		PrvekSeznamu* aktualni;
		PrvekSeznamu* zacatek;
		int pocet;
	public:
		TelefonniSeznam();
		~TelefonniSeznam();
		void pridejOsobu(Entity::Osoba o);
		std::string  najdiTelefon(std::string aJmeno)const;
		std::string najdiTelefon(int aId) const;

	};


}
#endif // !telefoniSeznam_H
