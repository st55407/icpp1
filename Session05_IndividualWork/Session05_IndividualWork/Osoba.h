#include <string>

#ifndef OSOBA_H
#define OSOBA_H

namespace Entity {
	class Osoba {
	private:
		std::string jmeno;
		std::string telefon;
		int id;

	public:
		Osoba();
		Osoba(std::string aJmeno, std::string aTelefon, int aId);
		~Osoba();
		void setJmeno(std::string aJmeno);
		void setTel(std::string aTelefon);
		void setId(int aId);
		std::string getJmeno() const;
		std::string getTel() const;
		int getId() const;


	};



}

#endif // !OSOBA_H
